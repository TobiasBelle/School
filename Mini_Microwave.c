/* ========================================
 *
 * Copyright Tobias van Belle, 2021
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include <math.h>

#define SECONDS(s) (CyDelay(1000*s))

_Bool finished = 0;
volatile uint16_t output[2];

//The interrupt
CY_ISR(BUTTON_InterruptHandler) {
    finished = 1;
    
    BUTTON_ClearInterrupt();
}
    
    //Function to turn on the motor
    void Preheating(int time,int repetitions){
        for (int i =0; i < repetitions; i++){
        PWM_1_Start();
        PIN_DIR_Write(1);
        PWM_1_WriteCompare(3000);
        SECONDS(time);
        PIN_DIR_Write(2);
        }
    }
    
    //Function that activates the buzzer when the motor is done
    void Notify(){
        BUZZER_Start();
        SECONDS(2);
        BUZZER_Stop();
    }
    
    //Function that resets the leds when the motor is finished
    void Restart(){
        LED_1_Write(0);
        LED_2_Write(0);
    }
    
    void getSwitchValue(){
        volatile uint16_t value = SW_1_Read();
        value += SW_2_Read();
        value += SW_3_Read();
        value += SW_4_Read();
        
        volatile uint16_t switchbar = 0;
        for(uint8_t i = 0; i < value * 2; i += 2) {
            switchbar += pow(2, i) + pow(2, i + 1);
        }
        output[0] = value;
        output[1] = switchbar;
    }
    
int main(void){

    CyGlobalIntEnable; /* Enable global interrupts. */
    
    ISR_BUTTON_StartEx(BUTTON_InterruptHandler);
    
    uint16_t value;
    for(;;){
        finished = 0;
        while (!finished) {
            getSwitchValue();
            LED_1_Write(output[1]);
            LED_2_Write(output[1] >> 5);
        }
        Restart();
        
        uint16_t ledbar = 0;
        for(uint8_t i = 0; i < output[0] * 2; i++) {
            ledbar += pow(2, i);
            Preheating(2, 2);
            LED_1_Write(ledbar);
            LED_2_Write(ledbar >> 5);
            CyDelay(2500);
        }
        Notify();
        Restart();
    }
}

/* [] END OF FILE */
